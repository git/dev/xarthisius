# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

MY_P=${PN}${PV}

DESCRIPTION="Program do rozliczeń podatkowych za rok 2011"
HOMEPAGE="http://www.e-pity.pl/"
SRC_URI="http://download.e-pity.pl/${MY_P}.air"

LICENSE="e-pity-2011"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND=">=dev-util/adobe-air-sdk-bin-2.6"

S=${WORKDIR}

src_unpack() {
   cp "${DISTDIR}"/${A} "${S}" || die
}

src_prepare() {
	cat <<- EOF > "${T}"/${MY_P}
		#/bin/sh
		airstart /opt/lib/${MY_P}.air
	EOF
}

src_install() {
	insinto /opt/lib
	doins ${MY_P}.air
	exeinto /opt/bin
	doexe "${T}"/${MY_P}
}
