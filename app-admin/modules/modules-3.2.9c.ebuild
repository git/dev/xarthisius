# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit versionator

MY_PV=$(get_version_component_range 1-3)
MY_P=${PN}-${MY_PV}

DESCRIPTION="Allows for dynamic modification of a user's environment via modulefiles"
HOMEPAGE="http://modules.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-lang/tcl"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${MY_P}

pkg_setup() {
	export _modules_path=/var/lib/Modules/${MY_PV}
}

src_configure() {
	# unfortunately {module,version}-path don't work properly
	./configure \
		--prefix=/var/lib \
		--mandir=/usr/share \
		--with-module-path=${_modules_path}/modulefiles \
		--with-version-path=${_modules_path}/versions \
		--disable-use-default \
		${EXTRA_ECONF} || die
}

src_install() {
	default
	dosym ${_modules_path} /var/lib/Modules/default
}

pkg_postinst() {
	echo
	einfo "Add this line at the end of /etc/bash/bashrc:"
	einfo "    [ -e \"${_modules_path}/init/bash\" ] && source \"${_modules_path}/init/bash\""
	echo
}
